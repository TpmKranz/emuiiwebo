import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import html from '@rollup/plugin-html';
import { terser } from 'rollup-plugin-terser';
import nodePolyfills from 'rollup-plugin-node-polyfills';
import globals from 'rollup-plugin-external-globals';
import packagejson from './package.json';

const API_URI = process.env.API_URI || "https://www.amiiboapi.com/api/amiibo/";
const NDEBUG = !!process.env.NDEBUG;

const plugins = [
  resolve({browser: true, preferBuiltins: true}),
  commonjs(),
  html({title: packagejson.description}),
  nodePolyfills(),
  globals({API_URI: `"${API_URI}"`, NDEBUG: NDEBUG.toString()})
];

NDEBUG && plugins.push(terser());

export default {
  plugins,
  input: 'src/index.js',
  output: {
    dir: 'public',
    format: 'es',
    sourcemap: true
  }
};
