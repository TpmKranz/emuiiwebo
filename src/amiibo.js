import {API_URI} from './flags.js';
const apiReq = new Request(API_URI);

export default function () {
  return fetch(apiReq)
    .then(res => res.json())
    .then(json => json["amiibo"] || reject("No amiibo found."));
};
