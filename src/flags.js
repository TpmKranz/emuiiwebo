import apiURI from 'API_URI';
import ndebug from 'NDEBUG';

export const API_URI = apiURI, NDEBUG = ndebug;
